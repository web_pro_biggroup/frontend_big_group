import type { ReceiptSalaryItem } from "@/types/ReceiptSalaryItem"
import { defineStore } from "pinia"
import { ref, watch } from "vue"
import { useLoadingStore } from "./loading"
import type { ReceiptSalary } from "@/types/ReceiptSalary"
import salaryDetail from "@/service/salaryDetail"


export const useReceiptSalary = defineStore('receiptSalary', () => {
  const loadingStore = useLoadingStore()
  const receiptSalaryDialog = ref(false)
  const receiptSalaryItems = ref<ReceiptSalaryItem[]>([])
  const receiptSalary = ref<ReceiptSalary>()
  initReceipt()

  function initReceipt() {
    receiptSalary.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
    
      paymentType: 'Cash'
    }
    receiptSalaryItems.value = []
  }

  watch(
    receiptSalaryItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )

  const calReceipt = function () {
    receiptSalary.value!.total = 0
    for (let i = 0; i < receiptSalaryItems.value.length; i++) {
      receiptSalary.value!.total +=
        receiptSalaryItems.value[i].salary
    }
  }

  const addReceiptItem = (newReceiptSalaryItemredient: ReceiptSalaryItem) => {
    receiptSalaryItems.value.push(newReceiptSalaryItemredient)
    console.log('add Item')
  }
  const deleteReceiptItem = (selectedItem: ReceiptSalaryItem) => {
    const index = receiptSalaryItems.value.findIndex((item) => item === selectedItem)
    receiptSalaryItems.value.splice(index, 1)
    console.log('del')
  }

  const removeItem = (item: ReceiptSalaryItem) => {
    const index = receiptSalaryItems.value.findIndex((ri) => ri === item)
    receiptSalaryItems.value.splice(index, 1)
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      await salaryDetail.addOrder(receiptSalary.value!, receiptSalaryItems.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }

  function showReceiptSalaryDialog() {
    //receiptSalary.value.receiptItemsIng = receiptItemsIng.value
    receiptSalaryDialog.value = true
  }

  async function getOrderIngredients() {
    loadingStore.doLoad()
    const res = await salaryDetail.getOrders()
    console.log(res.data)
    receiptSalaryItems.value = res.data
    loadingStore.finish()
  }

  async function showReceiptIngDialogById(selectedItem: ReceiptSalaryItem) {
    //const getItemIngredeintStore = await orderIngredientService.getOrder(selectedItem.id)
    //orderIngredientService.getOrder(selectedItem.id)
    console.log('show Item id --> ' + selectedItem.id)
    //console.log(selectedItem)
    //console.log(getItemIngredeintStore)
    //receiptSalaryItems.value = getItemIngredeintStore.data
    console.log()
  }

  return {
    receiptSalary,
    receiptSalaryItems,
    receiptSalaryDialog,
    addReceiptItem,
    deleteReceiptItem,
    calReceipt,
    removeItem,
    order,
    showReceiptSalaryDialog,
    initReceipt,
    getOrderIngredients,
    showReceiptIngDialogById
  }
})
