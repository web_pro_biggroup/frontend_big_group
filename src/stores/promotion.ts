import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
// import userService from '@/services/user'
// import type { User } from '@/types/User'
import promotionService from '@/service/promotion'
import type { Promotion } from '@/types/Promotion'
import { useCalculateStore } from './calculate'
import { useMessageStore } from './message'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const calculateStore = useCalculateStore()
  const checkMoneyProDialog = ref(false)
  const messageStore = useMessageStore()
  const promotionDialog = ref(false)
  const promotions = ref<Promotion[]>([])
  const promotionUsed = ref<Promotion>()
  const failUsePromotion1 = ref(false)
  const used = ref(false)
  const cp = ref(-1)
  const p = ref<Promotion>()
  const selectIndex = ref(-1)
  const numType = ref(-1)
  const Discount = ref(-1)
  let DiscountPercentage = -1
  const amount = ref()

  function reset(p: Promotion) {
    cp.value = -1
    numType.value = -1
    Discount.value = p.conditionCode.discountAmount
    DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.5
    amount.value = calculateStore.receiptPos!.totalAmount
  }
  function clearPromotion() {
    calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
    calculateStore.receiptPos!.totalAmount = calculateStore.amountCheckPromotion
    calculateStore.receiptPos!.promotion = undefined
  }

  function applyPromotion(p: Promotion) {
    console.log('เข้า')
    if (calculateStore.receiptPos!.promotion != undefined) {
      if (cp.value != calculateStore.receiptPos!.totalBefore) {
        //ถ้ามีการเพิ่มลดorder จะรีเซ็ต ตัวเปรียบเทียบtotal (cp => compare)
        reset(p)
      }
      // if(used.value == true) return
      if (p.type == 1) {
        //check มี promotionไหม
        if (numType.value == 2) {
          if (calculateStore.receiptPos!.totalBefore >= p.conditionCode.conditionAmount) {
            cp.value = calculateStore.receiptPos!.totalBefore
            calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
            Discount.value = p.conditionCode.discountAmount
            calculateStore.receiptPos!.total -= Discount.value
            calculateStore.receiptPos!.promotion = p
            numType.value = 1
          } else {
            console.log('ยอดชำระเงินไม่เพียงพอ')
            calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
            selectIndex.value = -1
            messageStore.showMessage('ยอดชำระเงินไม่เพียงพอ')
            calculateStore.receiptPos!.promotion = undefined
          }
        } else if (numType.value == 3) {
          calculateStore.receiptPos!.totalAmount = amount.value
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
          if (calculateStore.receiptPos!.total >= p.conditionCode.conditionAmount) {
            cp.value = calculateStore.receiptPos!.totalBefore
            Discount.value = p.conditionCode.discountAmount
            calculateStore.receiptPos!.total -= Discount.value
            calculateStore.receiptPos!.promotion = p
            numType.value = 1
          } else {
            console.log('ยอดชำระเงินไม่เพียงพอ')
            calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
            selectIndex.value = -1
            messageStore.showMessage('ยอดชำระเงินไม่เพียงพอ')
            calculateStore.receiptPos!.promotion = undefined
          }
        }
      } else {
        // ไม่มี promotion
        Discount.value = p.conditionCode.discountAmount
        if (calculateStore.receiptPos!.total >= p.conditionCode.conditionAmount) {
          cp.value = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.total -= Discount.value
          calculateStore.receiptPos!.promotion = p
          numType.value = 1
        } else {
          console.log('ยอดชำระเงินไม่เพียงพอ')
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
          selectIndex.value = -1
          messageStore.showMessage('ยอดชำระเงินไม่เพียงพอ')
          calculateStore.receiptPos!.promotion = undefined
        }
      }
    } else if (p.type == 2) {
      if (calculateStore.receiptPos!.promotion != undefined) {
        //check มี promotionไหม
        if (numType.value == 1) {
          cp.value = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.promotion = p
          numType.value = 2
          DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.3
          calculateStore.receiptPos!.total -= calculateStore.receiptPos!.totalBefore * 0.3
        } else if (numType.value == 3) {
          cp.value = calculateStore.receiptPos!.totalBefore
          DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.3
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.total -= calculateStore.receiptPos!.totalBefore * 0.3
          calculateStore.receiptPos!.promotion = p
          numType.value = 2
        }
      } else {
        cp.value = calculateStore.receiptPos!.totalBefore
        DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.3
        calculateStore.receiptPos!.total -= calculateStore.receiptPos!.totalBefore * 0.3
        calculateStore.receiptPos!.promotion = p
        numType.value = 2
      }
    } else if (p.type == 3) {
      if (calculateStore.receiptPos!.promotion != undefined) {
        if (numType.value == 1) {
          cp.value = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.total += Discount.value
          amount.value = calculateStore.receiptPos!.totalAmount
          DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.5
          // calculateStore.receiptPos!.totalAmount = Math.ceil(calculateStore.receiptPos!.totalAmount / 2)
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore * 0.5
          calculateStore.receiptPos!.promotion = p
          numType.value = 3
        } else if (numType.value == 2) {
          cp.value = calculateStore.receiptPos!.totalBefore
          calculateStore.receiptPos!.total += calculateStore.receiptPos!.totalBefore * 0.3
          amount.value = calculateStore.receiptPos!.totalAmount
          DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.5
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore * 0.5
          // calculateStore.receiptPos!.totalAmount = Math.ceil(calculateStore.receiptPos!.totalAmount / 2)
          calculateStore.receiptPos!.promotion = p
          numType.value = 3
        }
      } else {
        if (calculateStore.receiptPos!.totalAmount > 0) {
          cp.value = calculateStore.receiptPos!.totalBefore
          // amount.value = calculateStore.receiptPos!.totalAmount
          DiscountPercentage = calculateStore.receiptPos!.totalBefore * 0.5

          // if(calculateStore.receiptPos!.totalAmount > p.conditionCode.quantityRequired){
          // calculateStore.receiptPos!.totalAmount = Math.floor(calculateStore.receiptPos!.totalAmount/2)
          calculateStore.receiptPos!.total = calculateStore.receiptPos!.totalBefore * 0.5
          // }
          calculateStore.receiptPos!.promotion = p
          numType.value = 3
        }
      }
      calculateStore.calMemberDiscount()
    }
    numType.value = p.type
  }

  // function loopReceiptItems(){
  //   let A:number = 0,
  //       P:number = 0,
  //       totalA:number = 0,
  //       totalP:number = 0
  //   for (const receiptItem of calculateStore.receiptPos!.receiptItems!) {
  //     amount.value.push(receiptItem.unit)
  //     price.value.push(receiptItem.price)
  //     A += Math.ceil(receiptItem.unit/2)
  //     P += receiptItem.price * 0.5

  //     totalA = A
  //     totalP = P
  //   }
  //   calculateStore.receiptPos!.totalAmount! = totalA
  //   calculateStore.receiptPos!.total! = totalP
  // }

  function showPromotionDialog() {
    promotionDialog.value = true
  }

  function showCheckMoneyProDialog() {
    checkMoneyProDialog.value = true
  }

  async function getPromotion(id: number) {
    loadingStore.doLoad()
    const res = await promotionService.getPromotion(id)
    promotions.value = res.data
    loadingStore.finish()
  }

  //data function
  async function getPromotions() {
    const res = await promotionService.getPromotions()
    promotions.value = res.data
  }

  async function getPromotionByTypeId(typeId: number) {
    loadingStore.doLoad()
    const res = await promotionService.getPromotionByType(typeId)
    promotions.value = res.data
    loadingStore.finish()
  }

  async function savePromotion(promotion: Promotion) {
    loadingStore.doLoad()
    if (promotion.id < 0) {
      //add new
      console.log('post ' + JSON.stringify(promotion))
      const res = await promotionService.addPromotion(promotion)
    } else {
      //update
      console.log('patch ' + JSON.stringify(promotion))
      const res = await promotionService.updatePromotion(promotion)
    }
    await getPromotions()
    loadingStore.finish()
  }

  async function deletePromotion(promotion: Promotion) {
    loadingStore.doLoad()
    const res = await promotionService.removePromotion(promotion)
    await getPromotions()
    loadingStore.finish()
  }

  return {
    promotions,
    promotionUsed,
    promotionDialog,
    checkMoneyProDialog,
    selectIndex,
    numType,
    p,
    getPromotions,
    savePromotion,
    deletePromotion,
    getPromotion,
    getPromotionByTypeId,
    showPromotionDialog,
    applyPromotion,
    showCheckMoneyProDialog,
    clearPromotion
  }
})
