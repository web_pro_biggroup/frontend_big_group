import type { CheckIn_User } from '@/types/CheckInOut'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

export const useCheckInOut = defineStore('checkInOut', () => {
  const currentItem = ref<CheckIn_User | null>(null)
  const CheckInOutDialog = ref(false)

  const ShowCheckInOutDialog = (item: CheckIn_User) => {
    currentItem.value = item
    CheckInOutDialog.value = true
  }

  const state = (): CheckIn_User => ({
    id: 0,
    name: '',
    status: '',
    time: '',
    checked: false
  })

  // function ShowCheckInOutDialog() {
  //   CheckInOutDialog.value = true
  // }

  const headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Name',
      key: 'name'
    },
    {
      title: 'Status',
      key: 'status'
    },
    {
      title: 'Time',
      key: 'time',
      sorter: (a: CheckIn_User, b: CheckIn_User) => {
        // Implement logic to sort times based on your specific format (e.g., parse and compare)
        return 0 // Replace with appropriate comparison logic
      }
    },
    { title: 'Actions', key: 'actions', sortable: false }
  ]
  const checkin_user = ref<CheckIn_User[]>([
    {
      id: 1,
      name: 'โนบิตะ โนบิ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 2,
      name: 'มิซึริ คันโรจิ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 3,
      name: 'ชินอีจิ คุโด้',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 4,
      name: 'ทาเคชิ โกดะ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 5,
      name: 'ไจโกะ โกดะ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 6,
      name: 'โนบิซุเกะ โนบิ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 7,
      name: 'ชิซุกะ มินาโมโตะ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 8,
      name: 'เดคิซุงิ เดไซ',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 9,
      name: 'ซึเนโอะ โฮเนคาว่า',
      status: 'No',
      time: '',
      checked: false
    },
    {
      id: 10,
      name: 'เซวาชิ โนบิ',
      status: 'No',
      time: '',
      checked: false
    }
  ])

  // Function to handle checkbox changes (optional)
  const handleCheckboxChange = (user: CheckIn_User) => {
    // Perform actions based on checkbox state, e.g., update backend, toggle visibility, etc.
    console.log('Checkbox changed for user:', user.name, user.checked)

    if (user.checked) {
      user.time = new Date().toLocaleString() // Set the time to the current time
      user.status = 'Yes'
    } else {
      user.time = '' // Clear the time if unchecked
      user.status = 'No'
    }
  }

  const search = ref('')
  // Filtered checkin_user based on search query
  const filteredCheckinUser = computed(() => {
    const query = search.value.toLowerCase()
    return checkin_user.value.filter((user) => user.name.toLowerCase().includes(query))
  })

  return {
    headers,
    checkin_user,
    filteredCheckinUser,
    search,
    handleCheckboxChange,
    CheckInOutDialog,
    ShowCheckInOutDialog,
    currentItem
  }
})
