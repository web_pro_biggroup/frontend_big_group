// store/history.ts
import { defineStore } from 'pinia'

export const useHistory = defineStore('history', () => {
  type HistoryItem = {
    name: string
    id: number
    inStock: number
    Maximum: number
    Unit: string
    editedDate: Date
    changes: Record<string, string | number>
  }
  const editHistory = new Array<HistoryItem>()

  function addToHistory(item: HistoryItem) {
    editHistory.push(item)
  }

  function clearEditHistory() {
    editHistory.length = 0
  }

  return {
    editHistory,
    addToHistory,
    clearEditHistory
  }
})
