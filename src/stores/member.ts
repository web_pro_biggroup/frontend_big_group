import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useLoadingStore } from './loading'
import memberService from '@/service/member'
import member from '@/service/member'

// export const useMemberStore = defineStore('member', () => {
//   const members = ref<Member[]>([
//     { id: 1, name: 'อานี่ เธียร์รี่', tel: '0812345678',createdDate: new Date('01/01/2024'),},
//     { id: 2, name: 'โยอิจิ อิซากิ', tel: '0823456789',createdDate: new Date('01/01/2024'),},
//     { id: 3, name: 'เรสเค คิระ', tel: '0834567891',createdDate: new Date('01/01/2024'), },
//     { id: 4, name: 'เมกุรุ บาชิระ', tel: '0845678912',createdDate: new Date('01/01/2024'), },
//     { id: 5, name: 'เร็นสุเกะ คุนิกามิ', tel: '0856789123',createdDate: new Date('01/01/2024'), },
//     { id: 6, name: 'เหมะ ชิกิริ', tel: '0867891234',createdDate: new Date('01/01/2024'),},
//     { id: 7, name: 'จิงโกะ ไรจิ', tel: '0878912345' ,createdDate: new Date('01/01/2024'),},
//     { id: 8, name: 'วาตารุ คุน', tel: '0889123456' ,createdDate: new Date('01/01/2024'),},
//     { id: 9, name: 'จิน กากามารุ', tel: '0891234567',createdDate: new Date('01/01/2024'), },
//     { id: 10, name: 'ยูได อิมามูระ', tel: '0898765432',createdDate: new Date('01/01/2024'), }
//   ]);
export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([])

  const loadingStore = useLoadingStore()
  const statusMember = ref(false)

  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  async function getMemberByTel(tel: string) {
    loadingStore.doLoad()
    const res = await memberService.getMemberByTel(tel)
    currentMember.value = res.data
    members.value = res.data
    loadingStore.finish()
  }

  function clear() {
    currentMember.value = null
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    members.value = res.data
    loadingStore.finish()
  }

  //data function
  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember(member: Member) {
    loadingStore.doLoad()
    if (member.id < 0) {
      //add new
      console.log('post ' + JSON.stringify(member))
      const res = await memberService.addMember(member)
    } else {
      //update
      console.log('patch ' + JSON.stringify(member))
      const res = await memberService.updateMember(member)
    }
    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember(member: Member) {
    loadingStore.doLoad()
    const res = await memberService.removeMember(member)
    await getMembers()
    loadingStore.finish()
  }
  return {
    members,
    currentMember,
    statusMember,
    searchMember,
    clear,
    getMember,
    getMembers,
    saveMember,
    deleteMember,
    getMemberByTel
  }
})
