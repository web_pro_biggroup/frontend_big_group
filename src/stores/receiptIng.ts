import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
//mport type { ReceiptItem } from '@/type/ReceiptItemIng'
//import type { ReceiptIngStore } from '@/type/ReceiptIngStore'
//import type { ReceiptItemIng } from '@/type/ReceiptItemIng'
import { useLoadingStore } from './loading'
import type { ReceiptItemIng } from '@/types/ReceiptItemIng'
import type { ReceiptIngStore } from '@/types/ReceiptIngStore'
import orderIngredientService from '@/service/orderIngredient'
export const useReceiptIngStore = defineStore('receipt', () => {
  //const authStore = useAuthStore()
  //const memberStore = useMemberStore()
  const getItemIngredeintStore = ref<ReceiptItemIng>()
  const loadingStore = useLoadingStore()
  const receiptIngDialog = ref(false)
  const receiptItemsIngredient = ref<ReceiptItemIng[]>([])
  const receiptIng = ref<ReceiptIngStore>()
  initReceipt()

  function initReceipt() {
    receiptIng.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      total: 0,
      totalAmount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'Cash'
      // orderIngredientItems: {
      //   ingredientId: number,
      //   qty: number,
      // },
      //userId: authStore.getCurrentUser()!.id!,
    }
    receiptItemsIngredient.value = []
  }

  watch(
    receiptItemsIngredient,
    () => {
      calReceipt()
    },
    { deep: true }
  )

  const calReceipt = function () {
    receiptIng.value!.total = 0
    receiptIng.value!.totalAmount = 0
    receiptIng.value!.totalBefore = 0 // เพิ่มบรรทัดนี้เพื่อรีเซ็ตค่า totalBefore เป็น 0 ก่อนคำนวณใหม่
    for (let i = 0; i < receiptItemsIngredient.value.length; i++) {
      receiptIng.value!.total +=
        receiptItemsIngredient.value[i].price * receiptItemsIngredient.value[i].qty
      receiptIng.value!.totalAmount += receiptItemsIngredient.value[i].qty
      receiptIng.value!.totalBefore += // คำนวณ totalBefore ใหม่
        receiptItemsIngredient.value[i].price * receiptItemsIngredient.value[i].qty
    }
  }

  // const calReceipt = function () {
  //   receiptIng.value!.total = 0
  //   receiptIng.value!.totalAmount = 0
  //   for (let i = 0; i < receiptItemsIngredient.value.length; i++) {
  //     receiptIng.value!.total +=
  //       receiptItemsIngredient.value[i].price * receiptItemsIngredient.value[i].qty
  //     receiptIng.value!.totalAmount += receiptItemsIngredient.value[i].qty
  //     receiptIng.value!.totalBefore = receiptIng.value!.total
  //     receiptIng.value!.receivedAmount =
  //       receiptItemsIngredient.value[i].price * receiptItemsIngredient.value[i].qty
  //     console.log('cal')
  //   }
  // }

  const addReceiptItem = (newReceiptItemIngredient: ReceiptItemIng) => {
    receiptItemsIngredient.value.push(newReceiptItemIngredient)
    console.log('add Item')
  }
  const deleteReceiptItem = (selectedItem: ReceiptItemIng) => {
    const index = receiptItemsIngredient.value.findIndex((item) => item === selectedItem)
    receiptItemsIngredient.value.splice(index, 1)
    console.log('del')
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItemIng) => {
    selectedItem.qty++
    calReceipt()
    console.log('+')
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItemIng) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteReceiptItem(selectedItem)
      console.log('-')
    }
  }
  const removeItem = (item: ReceiptItemIng) => {
    const index = receiptItemsIngredient.value.findIndex((ri) => ri === item)
    receiptItemsIngredient.value.splice(index, 1)
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      await orderIngredientService.addOrder(receiptIng.value!, receiptItemsIngredient.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }

  function showReceiptIngDialog() {
    //receiptIng.value.receiptItemsIng = receiptItemsIng.value
    receiptIngDialog.value = true
  }

  async function getOrderIngredients() {
    loadingStore.doLoad()
    const res = await orderIngredientService.getOrders()
    console.log(res.data)
    receiptItemsIngredient.value = res.data
    loadingStore.finish()
  }

  async function showReceiptIngDialogById(selectedItem: ReceiptItemIng) {
    //const getItemIngredeintStore = await orderIngredientService.getOrder(selectedItem.id)
    //orderIngredientService.getOrder(selectedItem.id)
    console.log('show Item id --> ' + selectedItem.id)
    //console.log(selectedItem)
    //console.log(getItemIngredeintStore)
    //receiptItemsIngredient.value = getItemIngredeintStore.data
    console.log()
  }

  return {
    receiptIng,
    receiptItemsIngredient,
    receiptIngDialog,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    calReceipt,
    removeItem,
    order,
    showReceiptIngDialog,
    initReceipt,
    getOrderIngredients,
    showReceiptIngDialogById
  }
})
