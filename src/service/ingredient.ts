import type { Ingredient } from '@/types/CheckStock'
import http from './http'

function addNew(ingredient: Ingredient) {
  return http.post('/ingredient', ingredient)
}

function update(ingredient: Ingredient) {
  return http.patch(`/ingredient/${ingredient.id}`, ingredient)
}

function remove(ingredient: Ingredient) {
  return http.delete(`/ingredient/${ingredient.id}`)
}

function getUser(id: Number) {
  return http.get(`/ingredient/${id}`)
}

function getUsers() {
  return http.get('/ingredient')
}

export default { addNew, update, remove, getUser, getUsers }
