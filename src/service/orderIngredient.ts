import type { ReceiptItemIng } from '@/types/ReceiptItemIng'
import http from './http'
import type { ReceiptIngStore } from '@/types/ReceiptIngStore'
//import type { ReceiptItemIng } from '@/type/ReceiptItemIng'

type ReceiptDto = {
  orderIngredientItems: {
    ingredientId: number
    qty: number
  }[]
  //userId: number
}

function addOrder(ReceiptIngStore: ReceiptIngStore, receiptItemsIngredient: ReceiptItemIng[]) {
  const receiptDto: ReceiptDto = {
    orderIngredientItems: []
    //userId: 0
  }
  //receiptDto.userId = receipt.userId
  receiptDto.orderIngredientItems = receiptItemsIngredient.map((item) => {
    return {
      ingredientId: item.ingredientId,
      qty: item.qty
    }
  })
  //console.log(receiptDto)
  console.log('post Order')
  return http.post('/order-ingredient', receiptDto)
}

function getOrder(id: number) {
  return http.get(`/order-ingredient/${id}`)
}

function getOrders() {
  return http.get('/order-ingredient')
}

export default { addOrder, getOrder, getOrders }
