import type { Type } from '@/types/Type'
import http from './http'

function addType(type: Type) {
  return http.post('/type', type)
}

function updateType(type: Type) {
  return http.patch(`/type/${type.id}`, type)
}

function delType(type: Type) {
  return http.delete(`/type/${type.id}`)
}

function getType(id: number) {
  return http.get(`/type/${id}`)
}

function getTypes() {
  return http.get('/type')
}

export default { addType, updateType, delType, getType, getTypes }
