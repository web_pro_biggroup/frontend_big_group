import type { Expense } from '@/types/Expense'
import http from './http'

function addExpense(expense: Expense) {
  return http.post('/expense', expense)
}
// & { files: File[]
function updateExpense(expense: Expense) {
  return http.patch(`/expense/${expense.id}`, expense)
}

function delExpense(expense: Expense) {
  return http.delete(`/expense/${expense.id}`)
}

function getExpense(id: number) {
  return http.get(`/expense/${id}`)
}

function getExpenses() {
  return http.get('/expense')
}

export default { addExpense, updateExpense, delExpense, getExpense, getExpenses }
