import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(promotion: Promotion) {
  return http.post('/promotion', promotion)
}

function updatePromotion(promotion: Promotion) {
  return http.patch(`/promotion/${promotion.id}`, promotion)
}

function removePromotion(promotion: Promotion) {
  return http.delete(`/promotion/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotion/${id}`)
}

function getPromotions() {
  return http.get('/promotion')
}

function getPromotionByType(type: number) {
  return http.get('/promotion/type/' + type)
}

export default {
  addPromotion,
  updatePromotion,
  removePromotion,
  getPromotion,
  getPromotions,
  getPromotionByType
}
