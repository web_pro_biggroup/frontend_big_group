import type { History } from '@/types/History'
import http from './http'

function addNew(history: History) {
  return http.post('/history', history)
}

function update(history: History) {
  return http.patch(`/history/${history.id}`, history)
}

function remove(history: History) {
  return http.delete(`/history/${history.id}`)
}

function getUser(id: Number) {
  return http.get(`/history/${id}`)
}

function getUsers() {
  return http.get('/history')
}

export default { addNew, update, remove, getUser, getUsers }
