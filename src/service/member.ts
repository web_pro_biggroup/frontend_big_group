import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member) {
  return http.post('/member', member)
}

function updateMember(member: Member) {
  return http.patch(`/member/${member.id}`, member)
}

function removeMember(member: Member) {
  return http.delete(`/member/${member.id}`)
}

function getMember(id: number) {
  return http.get(`/member/${id}`)
}

function getMemberByTel(tel: string) {
  return http.get(`/member/tel/` + tel)
}

function getMembers() {
  return http.get('/member')
}

export default { addMember, updateMember, removeMember, getMember, getMembers, getMemberByTel }
