import type { ReceiptSalary } from '@/types/ReceiptSalary'
import type { ReceiptSalaryItem } from '@/types/ReceiptSalaryItem'
import http from './http'

type ReceiptDto = {
  salaryPaymentDetailItems: {
    empId?: number
    name?: string
    salary: number
  }[]
}

function addOrder(ReceiptSalary: ReceiptSalary, receiptSalaryItem: ReceiptSalaryItem[]) {
  const receiptDto: ReceiptDto = {
    salaryPaymentDetailItems: []
  }
  receiptDto.salaryPaymentDetailItems = receiptSalaryItem.map((item) => {
    return {
      empId: item.empId,
      name: item.name,
      salary: item.salary
    }
  })
  //console.log(receiptDto)
  console.log('post Order')
  return http.post('/salary-payment-detail', receiptDto)
}

function getOrder(id: number) {
  return http.get(`/salary-payment-detail/${id}`)
}

function getOrders() {
  return http.get('/salary-payment-detail')
}

export default { addOrder, getOrder, getOrders }
