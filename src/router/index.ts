import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import { useAuthStore } from '@/stores/auth'
import LayoutNavigation from '../components/LayoutMenu/LayoutNavigation.vue'
import LayoutAppbar from '../components/LayoutMenu/LayoutAppbar.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/pos/POSView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/posGraph',
      name: 'posGraph',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/pos/PosGraphView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/member',
      name: 'member',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/MemberView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/products',
      name: 'products',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/product/ProductView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/datailCkeckStock',
      name: 'datailCkeckStock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/DatailCheckStockView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/checkStock',
      name: 'checkStock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.

      components: {
        default: () => import('../views/checkStock/ShowCheckStockView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/check',
      name: 'check',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.

      components: {
        default: () => import('../views/checkStock/CheckStock.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/chartCheck',
      name: 'chartCheck',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.

      components: {
        default: () => import('../views/checkStock/ChartCheck.View.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/chartIngChart',
      name: 'chartIngChart',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.

      components: {
        default: () => import('../views/ingredientStore/IngredientStoreChart.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    // {
    //   path: '/test',
    //   name: 'test',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/testView.vue')
    // },
    {
      path: '/salaryPayment',
      name: 'salaryPayment',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/salary/SalaryPaymentView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/showSalaryPaymentDetail',
      name: 'showSalaryPaymentDetail',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/salary/ShowAllReceiptSalaryView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/storeBuy',
      name: 'storeBuy',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/StoreBuyView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/CheckInOutStatusView',
      name: 'CheckInOutStatusView',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/checkincheckout/CheckInOutStatusView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/checkInCheckOut',
      name: 'checkInCheckOut',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckInCheckOutStatus.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/checkedit',
      name: 'checkedit',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckEditView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/employee',
      name: 'employee',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/salary/EmployeeView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/paycost',
      name: 'paycost',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/costofutilities/PaycostView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },

    {
      path: '/receiptCostTable',
      name: 'receiptCostTable',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/costofutilities/ReceiptCostTable.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/paycostDialog',
      name: 'paycostDialog',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/costofutilities/PaycostDialog.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/ingStore',
      name: 'ingStore',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ingredientStore/IngredientsStore.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/showAllReceiptStoreIngredient',
      name: 'showAllReceiptStoreIngredient',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ingredientStore/ShowAllReceiptView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/userView',
      name: 'userView',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('../components/LayoutMenu/LayoutNavigation.vue'),
        header: () => import('../components/LayoutMenu/LayoutAppbar.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout'
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}
function canAccessRoute(routeName: string, isAdmin: boolean): boolean {
  // Allow everything for admin
  if (isAdmin) return true

  // Allowed routes for non-admin users
  const allowedRoutesForNonAdmins = ['home', 'pos', 'about']
  return allowedRoutesForNonAdmins.includes(routeName)
}

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore()
  const userEmail = authStore.getCurrentUserEmail()
  const isAdmin = userEmail === 'admin@admin.com'

  if (to.meta.requireAuth && !isLogin()) {
    next('/login')
  } else if (to.meta.requireAuth) {
    // Use isAdmin flag to determine access
    if (typeof to.name === 'string' && canAccessRoute(to.name, isAdmin)) {
      next()
    } else {
      // Redirect non-admin users trying to access restricted pages
      next('/login') // แก้ไขจาก next('/') เป็น next('/login') เพื่อเปลี่ยนเส้นทางไปยังหน้า login
    }
  } else {
    next()
  }
})

export default router
