type Promotion = {
  id: number
  condition: string
  conditionCode: PromotionCondition
  type: number
  startDate: Date
  endDate: Date
}

interface PromotionCondition {
  [key: string]: any
}

// interface PromotionConditionDiscount {
//   descript: string;
//   conditionAmount: number,
//   discountAmount: number
// }

// interface PromotionCondition1Free1 {
//   descript: string;
//   quantityRequired: number;
//   quantityFree: number;
// }

export type { Promotion, PromotionCondition }
