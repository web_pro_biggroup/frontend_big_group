import type { IngredientStore } from './IngredientStore'

type ReceiptItemIng = {
  id: number
  name: string
  price: number
  acc: number
  qty: number
  unit: string
  //receiptIngredintStoreId: ReceiptIngStore
  ingredientId: number
  ingredien?: IngredientStore
}
const defaultRecieptItemIng = {
  id: -1,
  name: '',
  price: 0,
  acc: 0,
  qty: 0,
  unit: '',
  //receiptIngredintStoreId: ReceiptIngStore
  ingredientId: -1,
  ingredient: null
}

export { type ReceiptItemIng }
