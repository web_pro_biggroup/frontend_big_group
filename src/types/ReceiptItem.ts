import type { Product } from './Product'
const defaultReceiptItem = {
  id: -1,
  name: '',
  price: 0,
  unit: 0,
  productId: -1,
  product: null,
  size: '',
  level: ''
}

type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  productId: number
  product?: Product
  size: string
  level: string
  // round: number;
}

export { type ReceiptItem, defaultReceiptItem }
