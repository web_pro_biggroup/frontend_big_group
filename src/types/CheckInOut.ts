type CheckIn_User = {
  id: number
  name: string
  status: string
  time: string
  checked: boolean
}

export { type CheckIn_User }
