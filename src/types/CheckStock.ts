type Ingredient = {
  id: number
  name: string
  inStock: number
  Maximum: number
  Unit: string
  createdDate: Date
  [key: string]: number | string | Date
}

export { type Ingredient }
