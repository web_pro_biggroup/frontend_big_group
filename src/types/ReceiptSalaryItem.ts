import type { Employee } from './Employee'

type ReceiptSalaryItem = {
  id: number
  name: string
  salary: number

  empId?: number
  employee?: Employee
}

export { type ReceiptSalaryItem }
