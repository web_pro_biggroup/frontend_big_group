type Employee = {
  id?: number
  name: string
  position: string
  salary: number
  image: string
  status: boolean
}

export { type Employee }
