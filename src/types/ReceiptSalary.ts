import type { ReceiptSalaryItem } from "./ReceiptSalaryItem"

type ReceiptSalary = {
  id: number
  createdDate: Date
  total: number
  paymentType: string

  receiptSalaryPaymentDetailItem?: ReceiptSalaryItem[]
  salaryPaymentDetailItems?: any[]
}

export { type ReceiptSalary }
