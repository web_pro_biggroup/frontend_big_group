type Member = {
  id: number
  name: string
  tel: string
  createdDate: Date
}

export type { Member }
