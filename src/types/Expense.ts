type Expense = {
  id?: number
  waterUnit: number
  waterPrice: number
  elecUnit: number
  elecPrice: number
  totalElecPrice: number
  totalWaterPrice: number
  rent: number
  total: number
  startDate: Date
}
export { type Expense }
